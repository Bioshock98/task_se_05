package ru.pyshinskiy.tm;

import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.command.*;

public class App {

    private final static Class[] CLASSES = new Class[] { ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectEditCommand.class, ProjectListCommand.class, ProjectRemoveCommand.class,
            ProjectSelectCommand.class, TaskAttachCommand.class, TaskClearCommand.class,
            TaskCreateCommand.class, TaskEditCommand.class, TaskListByProjectCommand.class, TaskListCommand.class,
            TaskRemoveCommand.class, TaskSelectCommand.class, TaskUnattachCommand.class, HelpCommand.class };

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES).start();
    }
}
