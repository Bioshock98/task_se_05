package ru.pyshinskiy.tm.repository;

import ru.pyshinskiy.tm.entity.Project;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class ProjectRepository extends AbstractRepository<Project> {

    private final Map<String, Project> projectMap = new LinkedHashMap<>();

    @Override
    public List<Project> findAll() {
        return (LinkedList<Project>)projectMap.values();
    }

    @Override
    public Project findOne(String id) throws Exception {
        if(id == null || id.isEmpty()) {
            throw new Exception();
        }
        return projectMap.get(id);
    }

    @Override
    public Project persist(Project project) {
        if(project == null) return null;
        return projectMap.put(project.getId(), project);
    }

    @Override
    public Project merge(Project project) throws Exception {
        if(project == null) return null;
        Project updatingProject = findOne(project.getId());
        updatingProject.setName(project.getName());
        updatingProject.setDescription(project.getDescription());
        updatingProject.setStartDate(project.getStartDate());
        updatingProject.setEndDate(project.getEndDate());
        return projectMap.put(updatingProject.getId(), updatingProject);
    }

    @Override
    public Project remove(String id) throws Exception {
        if(id == null || id.isEmpty()) {
            throw new Exception();
        }
        return projectMap.remove(id);
    }

    @Override
    public void removeAll() {
        projectMap.clear();
    }
}
