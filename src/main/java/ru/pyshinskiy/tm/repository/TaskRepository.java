package ru.pyshinskiy.tm.repository;

import ru.pyshinskiy.tm.entity.Task;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TaskRepository extends AbstractRepository<Task> {

    private final Map<String, Task> taskMap = new LinkedHashMap<>();

    @Override
    public List<Task> findAll() {
        return (LinkedList<Task>)taskMap.values();
    }

    @Override
    public Task findOne(String id) throws Exception {
        if(id == null || id.isEmpty()) {
            throw new Exception();
        }
        return taskMap.get(id);
    }

    @Override
    public Task persist(Task task) {
        if(task == null) return null;
        return taskMap.put(task.getId(), task);
    }

    @Override
    public Task merge(Task task) throws Exception {
        if(task == null) return null;
        Task updatingTask = findOne(task.getId());
        updatingTask.setName(task.getName());
        updatingTask.setProjectId(task.getProjectId());
        updatingTask.setDescription(task.getDescription());
        updatingTask.setStartDate(task.getStartDate());
        updatingTask.setEndDate(task.getEndDate());
        return taskMap.put(updatingTask.getId(), updatingTask);
    }

    @Override
    public Task remove(String id) throws Exception {
        if(id == null || id.isEmpty()) {
            throw new Exception();
        }
        return taskMap.remove(id);
    }

    @Override
    public void removeAll() {
        taskMap.clear();
    }
}
