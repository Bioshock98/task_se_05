package ru.pyshinskiy.tm.repository;

import java.util.List;

public abstract class AbstractRepository<T> {

    public abstract List<T> findAll();
    public abstract T findOne(String id) throws Exception;
    public abstract T persist(T abstractEntity);
    public abstract T merge(T abstractEntity) throws Exception;
    public abstract T remove(String id) throws Exception;
    public abstract void removeAll();
}
