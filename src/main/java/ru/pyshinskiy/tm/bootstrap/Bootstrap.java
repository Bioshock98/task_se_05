package ru.pyshinskiy.tm.bootstrap;

import ru.pyshinskiy.tm.command.*;
import ru.pyshinskiy.tm.repository.ProjectRepository;
import ru.pyshinskiy.tm.repository.TaskRepository;
import ru.pyshinskiy.tm.service.ProjectService;
import ru.pyshinskiy.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap {

    private final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final TaskService taskService = new TaskService(taskRepository);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void start() throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        String command = "";
        while(!"exit".equals(command)) {
            command = input.readLine();
            execute(command);
        }
    }

    public Bootstrap init(final Class[] classes) throws Exception {
        for(Class clazz : classes) {
            registry((AbstractCommand)clazz.newInstance());
        }
        return this;
    }

    private void execute(final String command) throws Exception {
        if(command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if(abstractCommand == null) {
            System.out.println("UNKNOW COMMAND");
            return;
        }
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) throws Exception {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new Exception();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new Exception();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }
}
