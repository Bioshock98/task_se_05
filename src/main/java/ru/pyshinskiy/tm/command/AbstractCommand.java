package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.bootstrap.Bootstrap;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;
    protected final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;
}
