package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Task;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_clear";
    }

    @Override
    public String description() {
        return "remove all projects";
    }

    @Override
    public void execute() throws Exception {
        for(Task task : bootstrap.getTaskService().findAll()) {
            if(task.getProjectId() != null) bootstrap.getTaskService().remove(task.getId());
        }
        bootstrap.getProjectService().removeAll();
        System.out.println("[ALL PROJECT REMOVED]");
    }
}
