package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.service.ProjectService;
import ru.pyshinskiy.tm.service.TaskService;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskListByProjectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_list_by_project";
    }

    @Override
    public String description() {
        return "show tasks by project";
    }

    @Override
    public void execute() throws Exception {
        ProjectService projectService = bootstrap.getProjectService();
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("[ENTER PROJECT ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        List<Task> tasksByProjectId = taskService.findAllByProjectId(projectId);
        printTasks(tasksByProjectId);
    }
}
