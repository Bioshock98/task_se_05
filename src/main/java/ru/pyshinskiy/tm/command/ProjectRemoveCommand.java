package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.service.ProjectService;
import ru.pyshinskiy.tm.service.TaskService;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_remove";
    }

    @Override
    public String description() {
        return "remove project and related tasks";
    }

    @Override
    public void execute() throws Exception {
        ProjectService projectService = bootstrap.getProjectService();
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        for(Task task : taskService.findAll()) {
            if(task.getProjectId().equals(projectId)) taskService.remove(task.getId());
        }
        projectService.remove(projectId);
        System.out.println("[PROJECT REMOVED]");
    }
}
