package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Task;

import static ru.pyshinskiy.tm.util.DateUtil.parseDateFromString;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_create";
    }

    @Override
    public String description() {
        return "create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER TASK NAME]");
        Task task = new Task(input.readLine(), null);
        System.out.println("ENTER TASK DESCRIPTION");
        task.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        task.setEndDate(parseDateFromString(input.readLine()));
        bootstrap.getTaskService().persist(task);
        System.out.println("[OK]");
        bootstrap.getTaskService().persist(task);
    }
}
