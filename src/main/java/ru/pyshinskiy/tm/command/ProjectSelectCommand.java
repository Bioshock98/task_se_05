package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.service.ProjectService;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProject;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public class ProjectSelectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_select";
    }

    @Override
    public String description() {
        return "show project info";
    }

    @Override
    public void execute() throws Exception {
        ProjectService projectService = bootstrap.getProjectService();
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Project project = projectService.findOne(projectId);
        printProject(project);
    }
}
