package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Project;

import static ru.pyshinskiy.tm.util.DateUtil.parseDateFromString;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_create";
    }

    @Override
    public String description() {
        return "create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        Project project = new Project(input.readLine());
        System.out.println("ENTER PROJECT DESCRIPTION");
        project.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        project.setEndDate(parseDateFromString(input.readLine()));
        bootstrap.getProjectService().persist(project);
        System.out.println("[OK]");
    }
}
