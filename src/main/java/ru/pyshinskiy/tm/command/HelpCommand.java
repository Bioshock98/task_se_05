package ru.pyshinskiy.tm.command;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "show all commands";
    }

    @Override
    public void execute() throws Exception {
        for(final AbstractCommand command : bootstrap.getCommands()) {
            System.out.println(command.command() + ": " +
                    command.description());
        }
    }
}
