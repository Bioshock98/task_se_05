package ru.pyshinskiy.tm.command;

public class TaskClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_clear";
    }

    @Override
    public String description() {
        return "remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASK]");
        bootstrap.getTaskService().removeAll();
        System.out.println("[ALL TASKS REMOVED]");
    }
}
