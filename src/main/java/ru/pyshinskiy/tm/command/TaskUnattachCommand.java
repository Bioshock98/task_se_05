package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.service.TaskService;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskUnattachCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_unattach";
    }

    @Override
    public String description() {
        return "unattach task from project";
    }

    @Override
    public void execute() throws Exception {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[UNATTACH TASK]");
        printTasks(taskService.findAll());
        System.out.println("ENTER TASK ID");
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        taskService.findOne(taskId).setProjectId(null);
        System.out.println("[OK]");
    }
}
