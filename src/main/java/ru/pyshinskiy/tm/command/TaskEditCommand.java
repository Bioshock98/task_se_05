package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.service.TaskService;

import static ru.pyshinskiy.tm.util.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskEditCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_edit";
    }

    @Override
    public String description() {
        return "edit existing task";
    }

    @Override
    public void execute() throws Exception {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Task task = taskService.findOne(taskId);
        System.out.println("[ENTER TASK NAME]");
        Task anotherTask = new Task(input.readLine(), null);
        anotherTask.setId(task.getId());
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherTask.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        anotherTask.setEndDate(parseDateFromString(input.readLine()));
        System.out.println("[OK]");
        taskService.merge(anotherTask);
    }
}
