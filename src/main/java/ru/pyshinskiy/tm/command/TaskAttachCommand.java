package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.service.ProjectService;
import ru.pyshinskiy.tm.service.TaskService;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskAttachCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_attach";
    }

    @Override
    public String description() {
        return "attach task to project";
    }

    @Override
    public void execute() throws Exception {
        ProjectService projectService = bootstrap.getProjectService();
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[ATTACH TASK]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Project project = projectService.findOne(projectId);
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Task task = taskService.findOne(taskId);
        task.setProjectId(project.getId());
        System.out.println("[OK]");
    }
}
