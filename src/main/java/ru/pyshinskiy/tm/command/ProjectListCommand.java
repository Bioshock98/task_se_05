package ru.pyshinskiy.tm.command;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_list";
    }

    @Override
    public String description() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        printProjects(bootstrap.getProjectService().findAll());
        System.out.println("[OK]");
    }
}
