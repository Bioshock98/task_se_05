package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.service.TaskService;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_remove";
    }

    @Override
    public String description() {
        return "remove task";
    }

    @Override
    public void execute() throws Exception {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        taskService.remove(taskId);
        System.out.println("[TASK REMOVED]");
    }
}
