package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.service.ProjectService;

import static ru.pyshinskiy.tm.util.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public class ProjectEditCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_edit";
    }

    @Override
    public String description() {
        return "edit existing project";
    }

    @Override
    public void execute() throws Exception {
        ProjectService projectService = bootstrap.getProjectService();
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Project project = projectService.findOne(projectId);
        System.out.println("ENTER NAME");
        Project anotherProject = new Project(input.readLine());
        anotherProject.setId(project.getId());
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherProject.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        anotherProject.setEndDate(parseDateFromString(input.readLine()));
        projectService.merge(anotherProject);
        System.out.println("[OK]");
    }
}
