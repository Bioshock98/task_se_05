package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.service.TaskService;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTask;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskSelectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_select";
    }

    @Override
    public String description() {
        return "show task info";
    }

    @Override
    public void execute() throws Exception {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[TASK SELECT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Task task = taskService.findOne(taskId);
        printTask(task);
    }
}
