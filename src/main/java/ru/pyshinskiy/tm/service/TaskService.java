package ru.pyshinskiy.tm.service;

import ru.pyshinskiy.tm.repository.TaskRepository;
import ru.pyshinskiy.tm.entity.Task;

import java.util.LinkedList;
import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAllByProjectId(String projectId) {
        List<Task> tasksByProject = new LinkedList<>();
        for(Task task : taskRepository.findAll()) {
            if(task.getProjectId().equals(projectId)) tasksByProject.add(task);
        }
        return tasksByProject;
    }

    public Task findOne(String id) throws Exception {
        return taskRepository.findOne(id);
    }

    public Task persist(Task task) {
        return taskRepository.persist(task);
    }

    public Task merge(Task task) throws Exception {
        return taskRepository.merge(task);
    }

    public Task remove(String id) throws Exception {
        return taskRepository.remove(id);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public String getIdByNumber(int number) {
        List<Task> tasks = findAll();
        for(Task task : findAll()) {
            if(task.getId().equals(tasks.get(number).getId())) return task.getId();
        }
        return null;
    }
}
