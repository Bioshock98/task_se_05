package ru.pyshinskiy.tm.service;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project findOne(String id) throws Exception {
        return projectRepository.findOne(id);
    }

    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project persist(Project project) {
        return projectRepository.persist(project);
    }

    public Project merge(Project project) throws Exception {
        return projectRepository.merge(project);
    }

    public Project remove(String id) throws Exception {
        return projectRepository.remove(id);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public String getIdByNumber(int number) {
        List<Project> tasks = (LinkedList<Project>)findAll();
        for(Project project : findAll()) {
            if(project.getId().equals(tasks.get(number).getId())) return project.getId();
        }
        return null;
    }
}
