package ru.pyshinskiy.tm.util.entity;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.DateUtil.parseDateToString;

public class EntityUtil {
    public static void printProjects(Collection<Project> projects) {
        for (int i = 0; i < projects.size(); i++) {
            final Project project = ((LinkedList<Project>)projects).get(i);
            System.out.println((i + 1) + "." + " " + project.getName());
        }
    }

    public static void printProject(Project project) {
        StringBuilder formatedProject = new StringBuilder();
        formatedProject.append("project name: ");
        formatedProject.append(project.getName());
        formatedProject.append("\nproject description: ");
        formatedProject.append(project.getDescription());
        formatedProject.append("\nstart date: ");
        formatedProject.append(parseDateToString(project.getStartDate()));
        formatedProject.append("\nend date: ");
        formatedProject.append(parseDateToString(project.getEndDate()));
        System.out.println(formatedProject);
    }

    public static void printTasks(List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i + 1) + "." + " " + tasks.get(i).getName());
        }
    }

    public static void printTask(Task task) {
        StringBuilder formatedTask = new StringBuilder();
        formatedTask.append("task name: ");
        formatedTask.append(task.getName());
        formatedTask.append("\ntask description: ");
        formatedTask.append(task.getDescription());
        formatedTask.append("\nstart date: ");
        formatedTask.append(parseDateToString(task.getStartDate()));
        formatedTask.append("\nend date: ");
        formatedTask.append(parseDateToString(task.getEndDate()));
        System.out.println(formatedTask);
    }
}
